const globImporter = require('node-sass-glob-importer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = (env, argv) => {
  const isDevMode = argv.mode === "development";
  return {
    mode: isDevMode ? "development" : "production",
    devtool: isDevMode ? "source-map" : false,
    entry: {
      index: ["./src/index.js"],
    },
    module: {
      rules: [
        {
          test: /\.scss$/, 
          use: [
            {
              loader: MiniCssExtractPlugin.loader
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true,
                modules: false,
                localIdentName: "[local]___[hash:base64:5]"
              }
            },
            {
              loader: "postcss-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: "sass-loader",
              options: {
                importer: globImporter(),
                sourceMap: true,
              }
            }
          ]
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: "babel-loader",
            options: {
              presets: [
                ["@babel/preset-env", { modules: false }]
              ]
            }
          }
        },
        {
          test: /\.(png|jpe?g|gif)$/i,
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
          },
        },
      ]
    },
    output: {
      path: isDevMode ? path.resolve(__dirname, "dev") : path.resolve(__dirname, "dist"),
      filename: "[name].min.js",
      publicPath: "./dist"
    },
    plugins: [
      new MiniCssExtractPlugin(),
      /*new BrowserSyncPlugin({
        host: "localhost",
        port: 3000,
        proxy: "http://cornell.local/"
      })*/
    ]
  };
};
