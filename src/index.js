export function VanillaSlideToggle() {

    var slideUpTimeout,
        slideDownTimeout,
        $this = this;
    
    this.animationFinished = true;
 
    this.slideUp = function(target) {
            $this.animationFinished = false;
            var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
            target.style.transitionProperty = 'height, margin, padding';
            target.style.transitionDuration = duration + 'ms';
            target.style.boxSizing = 'border-box';
            target.style.height = target.offsetHeight + 'px';
            target.offsetHeight;
            target.style.overflow = 'hidden';
            target.style.height = 0;
            target.style.paddingTop = 0;
            target.style.paddingBottom = 0;
            target.style.marginTop = 0;
            target.style.marginBottom = 0;

            clearTimeout(slideDownTimeout);
            clearTimeout(slideUpTimeout);

            slideUpTimeout = window.setTimeout(function () {
                $this.animationFinished = true;
                target.style.display = 'none';
                target.style.removeProperty('height');
                target.style.removeProperty('padding-top');
                target.style.removeProperty('padding-bottom');
                target.style.removeProperty('margin-top');
                target.style.removeProperty('margin-bottom');
                target.style.removeProperty('overflow');
                target.style.removeProperty('z-index');
                target.style.removeProperty('transition-duration');
                target.style.removeProperty('transition-property'); //alert("!");
            }, duration);
    };

    this.slideDown = function(target) {
            $this.animationFinished = false;
            var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
            target.style.removeProperty('display');
            var display = window.getComputedStyle(target).display;
            if (display === 'none') display = 'flex';
            target.style.display = display;
            var height = target.offsetHeight;
            target.style.overflow = 'hidden';
            target.style.height = 0;
            target.style.paddingTop = 0;
            target.style.paddingBottom = 0;
            target.style.marginTop = 0;
            target.style.marginBottom = 0;
            target.offsetHeight;
            target.style.boxSizing = 'border-box';
            target.style.transitionProperty = "height, margin, padding";
            target.style.transitionDuration = duration + 'ms';
            target.style.height = height + 'px';
            target.style.zIndex = '2';
            target.style.removeProperty('padding-top');
            target.style.removeProperty('padding-bottom');
            target.style.removeProperty('margin-top');
            target.style.removeProperty('margin-bottom');

            clearTimeout(slideDownTimeout);
            clearTimeout(slideUpTimeout);

            slideDownTimeout = window.setTimeout(function () {
                $this.animationFinished = true;
                target.style.removeProperty('height');
                target.style.removeProperty('overflow');
                target.style.removeProperty('transition-duration');
                target.style.removeProperty('transition-property');
            }, duration);
    };

    this.slideToggle = function(target) {

        var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;

        if (window.getComputedStyle(target).display === 'none') {

            if ($this.animationFinished) {
                $this.slideDown(target, duration);
            }
        } else {
            if ($this.animationFinished) {
                $this.slideUp(target, duration);
            }
        }
    };

    this.clearStyles = function (target) {
        target.removeAttribute('style');
    }
};

export function VanillaFadeToggle() {

    var fadeOutTimeout,
        fadeInTimeout,
        $this = this;

    this.animationFinished = true;

    this.fadeOut = function (target) {
            $this.animationFinished = false;
            var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
            target.style.transitionProperty = 'opacity';
            target.style.transitionDuration = duration + 'ms';
            target.style.boxSizing = 'border-box';
            target.style.opacity = target.style.opacity;
            target.style.overflow = 'hidden';
            target.style.opacity = 0;

            clearTimeout(fadeInTimeout);
            clearTimeout(fadeOutTimeout);

            fadeOutTimeout = window.setTimeout(function () {
                $this.animationFinished = true;
                target.style.display = 'none';
                target.style.removeProperty('opacity');
                target.style.removeProperty('overflow');
                target.style.removeProperty('transition-duration');
                target.style.removeProperty('transition-property'); //alert("!");
            }, duration);
    };

    this.fadeIn = function (target) {
        console.log('boosh');
            $this.animationFinished = false;
            var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
            target.style.removeProperty('display');
            var display = window.getComputedStyle(target).display;
            if (display === 'none') display = 'block';
            target.style.display = display;
            var opacity = window.getComputedStyle(target).opacity;
            target.style.opacity = opacity;
            target.style.overflow = 'hidden';
            target.style.boxSizing = 'border-box';
            target.style.transitionProperty = "opacity";
            target.style.transitionDuration = duration + 'ms';
            target.style.opacity = 1;

            clearTimeout(fadeInTimeout);
            clearTimeout(fadeOutTimeout);

            fadeInTimeout = window.setTimeout(function () {
                $this.animationFinished = true;
                target.style.removeProperty('overflow');
                target.style.removeProperty('transition-duration');
                target.style.removeProperty('transition-property');
            }, duration);
    };

    this.fadeToggle = function (target) {
        var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;

        //if the target is already 'display: none; via css, (see .getComputedStyle) fade in. else, fade out
        if (window.getComputedStyle(target).display === 'none') {
            if ($this.animationFinished) {
                return this.fadeIn(target, duration);
            }
        } else {
            if ($this.animationFinished) {
                return this.fadeOut(target, duration);
            }
        }
    };

    this.clearStyles = function (target) {
        target.removeAttribute('style');
    }
};
